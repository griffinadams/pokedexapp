Pokedex.RootView.prototype.addToyToList = function (toy) {
	var $toyLi = $('<li>').html(toy.attributes.name + " -- " + toy.attributes.happiness + " -- " + toy.attributes.price);
	$toyLi.data("toy-id", toy.id);
	$toyLi.data("pokemon-id", toy.attributes.pokemon_id);
	$toyLi.addClass('toy');
	return $toyLi;
};

Pokedex.RootView.prototype.renderToyDetail = function (toy) {
	var $detail = $('<div>');

	_.each(toy.attributes, function (value, key, list) {
		$div = $("<div>").addClass("detail");

		if (key === "image_url") {
			$div.append($("<img>").attr("src", value));
		} else {
			$div.html(key + "––" + value);
		}
		$detail.append($div);
	});

	var $select = $('<select>');
	$select.data('pokemon-id', toy.attributes.pokemon_id);
	$select.data('toy-id', toy.id);

	this.pokes.each(function (poke) {
		var $newOption = $('<option>');
		$newOption.text(poke.attributes.name);
		$newOption.val(poke.attributes.id);
		if (poke.attributes.id === toy.attributes.pokemon_id) {
			$newOption.prop("selected", true);
		};
		$select.append($newOption);
	});

	$detail.append($select);

	this.$toyDetail.html($detail);
};

Pokedex.RootView.prototype.selectToyFromList = function (event) {
	var $toy = $(event.currentTarget);
	var toy_id = $toy.data("toy-id");
	var pokemon_id = $toy.data("pokemon-id");

	var toyModel = this.pokes.get(pokemon_id).toys().get(toy_id);
	this.renderToyDetail(toyModel);
};
