Pokedex.RootView.prototype.renderPokemonDetail = function (pokemon) {
	var $detail = $("<div>")
	var $img = $("<img>").attr("src", pokemon.attributes.image_url);
	var $imageDiv = $("<div>").addClass('detail');
	$imageDiv.append($img);
	$detail.append($imageDiv);
	_.each(pokemon.attributes, function (value, key, list) {
		if (key !== "image_url") {
			$detail.append($("<div>").html(key + " –– " + value).addClass("detail"));
		}
	});

	pokemon.fetch( { success: function () {
		var $toys = this.renderToysList(pokemon.toys());
		$detail.append($toys);
	}.bind(this) });

	this.$pokeDetail.html($detail);
};

Pokedex.RootView.prototype.selectPokemonFromList = function (event) {
	var pokemonId = $(event.currentTarget).data('id');
	var $pokemon = this.pokes.get(pokemonId);
	this.renderPokemonDetail($pokemon);
};
