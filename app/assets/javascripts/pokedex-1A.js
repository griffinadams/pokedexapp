Pokedex.RootView.prototype.addPokemonToList = function (pokemon) {
	var $pokemon = $("<li>");
	$pokemon.addClass("poke-list-item");
	$pokemon.data('id', pokemon.attributes.id);
	$pokemon.text(pokemon.attributes.name + ", " + pokemon.attributes.poke_type);
	this.$pokeList.append($pokemon);
};

Pokedex.RootView.prototype.refreshPokemon = function () {
	this.pokes.fetch({
		success: function () {
			this.pokes.each(function (poke) {
				this.addPokemonToList(poke)
			}.bind(this))
		}.bind(this)
	})
};
