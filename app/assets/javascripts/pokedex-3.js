Pokedex.RootView.prototype.reassignToy = function (event) {
	var oldPokeId = $(event.currentTarget).data('pokemon-id');
	var oldPoke = this.pokes.get(oldPokeId);

	var newPokeId = $(event.target).val();
	var newPoke = this.pokes.get(newPokeId);

	var toyId = $(event.currentTarget).data('toy-id');
	var toy = oldPoke.toys().get(toyId);

	toy.set('pokemon_id', newPokeId);

	toy.save({}, { success: function () {
		oldPoke.toys().remove(toy);
		this.renderPokemonDetail(oldPoke);
		this.$toyDetail.empty();
	}

	.bind(this)});
};

Pokedex.RootView.prototype.renderToysList = function (toys) {
	this.$pokeDetail.find(".toys").empty();

	var $toys = $('<ul>').addClass('toys');
	$toys.append("<h3>Toys</h3>");

	toys.each (function (toy) {
		$toys.append(this.addToyToList(toy));
	}.bind(this))

	return $toys
};
