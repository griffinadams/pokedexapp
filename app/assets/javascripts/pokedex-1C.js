Pokedex.RootView.prototype.createPokemon = function (attrs, callback) {
	var newPokemon = new Pokedex.Models.Pokemon();
	newPokemon.set(attrs);

	newPokemon.save( {}, { success: function (data) {
		this.addPokemonToList(newPokemon);
		this.pokes.add(newPokemon);
		callback(newPokemon);
	}.bind(this)});
};

Pokedex.RootView.prototype.submitPokemonForm = function (event) {
	event.preventDefault();
	var newPokeattrs = $(event.currentTarget).serializeJSON().pokemon;
	this.createPokemon(newPokeattrs, this.renderPokemonDetail.bind(this));
};
