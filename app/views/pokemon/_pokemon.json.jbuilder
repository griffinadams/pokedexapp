json.(pokemon, :id, :attack, :defense, :image_url, :moves, :name, :poke_type)

if display_toys
	json.toys toys, partial: 'toys/toy', as: :toy
end
